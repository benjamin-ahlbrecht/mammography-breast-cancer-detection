# Mammography Breast Cancer Detection

![Test](/assets/breast-cancer-detection.svg)

 Mammograms are routine screenings recommended for woman over the age of 40 in the hopes of detecting breast cancer early. Given the recent advances in deep learning, specifically convolutional neural networks (CNNs), there has been increased interest in creating computer-aided detection and diagnosis (CAD) systems to help detect and segment cancerous tissue in the hopes of both improving patient outcomes and reducing costs and labor associated with reading and interpreting mammograms.

## Background
- For background information, see my post at https://benjamin-ahlbrecht.dev/projects/breast-cancer-detection

## Installation and Usage
It is highly suggested to train, evaluate, and experiment with the model directly on [Kaggle](https://www.kaggle.com/) since the environment is prepared.
- [Version 1](https://www.kaggle.com/code/benjaminahlbrecht/rsna-mammography-training)
- [Version 2](https://www.kaggle.com/code/benjaminahlbrecht/rsna-mammography-training-v2)
